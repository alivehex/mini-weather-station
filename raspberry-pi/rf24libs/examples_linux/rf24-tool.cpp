/*
 Copyright (C) 2011 J. Coliz <maniacbug@ymail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.

 03/17/2013 : Charles-Henri Hallard (http://hallard.me)
              Modified to use with Arduipi board http://hallard.me/arduipi
						  Changed to use modified bcm2835 and RF24 library
TMRh20 2014 - Updated to work with optimized RF24 Arduino library

 */

/**
 * Example RF Radio Ping Pair
 *
 * This is an example of how to use the RF24 class on RPi, communicating to an Arduino running
 * the GettingStarted sketch.
 */
#include <stdio.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <RF24/RF24.h>
#include <mysql/mysql.h>

using namespace std;
//
// Hardware configuration
// Configure the appropriate pins for your connections

/****************** Raspberry Pi ***********************/

// Radio CE Pin, CSN Pin, SPI Speed
// See http://www.airspayce.com/mikem/bcm2835/group__constants.html#ga63c029bd6500167152db4e57736d0939 and the related enumerations for pin information.

// Setup for GPIO 22 CE and CE0 CSN with SPI Speed @ 4Mhz
//RF24 radio(RPI_V2_GPIO_P1_22, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ);

// NEW: Setup for RPi B+
//RF24 radio(RPI_BPLUS_GPIO_J8_15,RPI_BPLUS_GPIO_J8_24, BCM2835_SPI_SPEED_8MHZ);

// Setup for GPIO 15 CE and CE0 CSN with SPI Speed @ 8Mhz
//RF24 radio(RPI_V2_GPIO_P1_15, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ);

// RPi generic:
RF24 radio(22,0);

/*** RPi Alternate ***/
//Note: Specify SPI BUS 0 or 1 instead of CS pin number.
// See http://tmrh20.github.io/RF24/RPi.html for more information on usage

//RPi Alternate, with MRAA
//RF24 radio(15,0);

//RPi Alternate, with SPIDEV - Note: Edit RF24/arch/BBB/spi.cpp and  set 'this->device = "/dev/spidev0.0";;' or as listed in /dev
//RF24 radio(22,0);


/****************** Linux (BBB,x86,etc) ***********************/

// See http://tmrh20.github.io/RF24/pages.html for more information on usage
// See http://iotdk.intel.com/docs/master/mraa/ for more information on MRAA
// See https://www.kernel.org/doc/Documentation/spi/spidev for more information on SPIDEV

// Setup for ARM(Linux) devices like BBB using spidev (default is "/dev/spidev1.0" )
//RF24 radio(115,0);

//BBB Alternate, with mraa
// CE pin = (Header P9, Pin 13) = 59 = 13 + 46 
//Note: Specify SPI BUS 0 or 1 instead of CS pin number. 
//RF24 radio(59,0);

/********** User Config *********/
// Assign a unique identifier for this node, 0 or 1
bool radioNumber = 1;

/********************************/

// Radio pipe addresses for the 2 nodes to communicate.
const uint8_t pipes[][6] = {"1Node", "2Node"};

void rf24_lowlevel_init(void);
int rf24_write(char *buf, int len);
int rf24_read(char *buf, unsigned long timeout_ms);
int rf24_cmd(char *txbuf, char *rxbuf, int txlen, char *err);
void finish_with_error(MYSQL *con);
int delete_database(const char *db_name);
int store_to_database(const char *db_name, const char *tb_name, const char *user_name, const char *user_pw, 
	int indoor_temp_data, int outdoor_temp_data, int humi_data, int wind_data, int err, const char *err_msg);
int read_indoor_temperature(void);

#define WS_CONNECT_FAILED		-1
#define WS_RESPONSE_TIMEOUT		-2

int main(int argc, char** argv) {

	bool role_ping_out = true, role_pong_back = false;
	bool role = role_pong_back;
	
	char txbuf[32];
	char rxbuf[32]; 
	char err_msg[128];
	int ret, retry;
	int in_temp_data, out_temp_data, humi_data, wind_data;
	char db_name[128] = "wsdb";
	char tb_name[128] = "WSHomeData";
	bool remove_database = false;
	char usr_name[128] = "root";
	char usr_pw[32] = "198579";
	
	for(int i = 1; i < argc; i ++) {
		if (argv[i][0] == '-') {
			if (strcmp(&argv[i][1], "db") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						printf("error: no database name input\n");
						return -1;
					} else { 
						sprintf(db_name, "%s", argv[i+1]);
					}
				}
			} 
			else if (strcmp(&argv[i][1], "tb") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						printf("error: no table name input\n");
						return -1;
					} else { 
						sprintf(tb_name, "%s", argv[i + 1]);
					}
				}			
			} 
			else if (strcmp(&argv[i][1], "rmdb") == 0) { 		 
				remove_database = true;
			}
			else if (strcmp(&argv[i][1], "u") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						printf("error: no database user name input\n");
						return -1;
					} else { 
						sprintf(usr_name, "%s", argv[i + 1]);
					}
				}
			} 
			else if (strcmp(&argv[i][1], "p") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						printf("error: no database user password input\n");
						return -1;
					} else { 
						sprintf(usr_pw, "%s", argv[i + 1]);
					}
				}
			} 	
			else {
				printf("error: unrecognized command line option '%s'\n", argv[i]);
				return -1;
			} 
		}
	}
	
	//printf("db_name='%s', tb_name='%s', user='%s', password='%s'\n", db_name, tb_name, usr_name, usr_pw);
	
	if (remove_database) {
		delete_database(db_name);
		return 0;
	}
	
	in_temp_data = read_indoor_temperature();
	if (in_temp_data == -1) {
		printf("indoor temperature data error\n");
	} else {
		printf("indoor temperature %0.1f\n", (float)in_temp_data / 16.0);
	}
	
	rf24_lowlevel_init();
	
	/* read temperature sensor data, should be 2 bytes long */
	strcpy(txbuf, "t");
	retry = 3;
	do {
		ret = rf24_cmd(txbuf, rxbuf, strlen(txbuf), err_msg);
	} while ( (ret < 0) && (-- retry) );
	
	if ( (ret == WS_CONNECT_FAILED) || (ret == WS_RESPONSE_TIMEOUT) || (retry == 0) ) {
		// we lost weather station, in_temp_data may be good 
		store_to_database(db_name, tb_name, usr_name, usr_pw, in_temp_data, -1, -1, -1, 1, (const char *)err_msg);
		return -1;
	}
	uint16_t tbuf;
	memcpy(&tbuf, rxbuf, 2);
		
	if ( (ret != 2) || (tbuf == 0xffff) ) {
		// something wrong with the data
		out_temp_data = -1;
		printf("outdoor temperature data error\n");
	} else {
		out_temp_data = tbuf;
		printf("outdoor temperature %0.1f\n", (float)out_temp_data / 16.0);
	}
	
	/* read humidity sensor data, should be 1 bytes long */
	strcpy(txbuf, "h");
	retry = 3;
	do {
		ret = rf24_cmd(txbuf, rxbuf, strlen(txbuf), err_msg);
	} while ( (ret < 0) && (-- retry) );
	
	if ( (ret == WS_CONNECT_FAILED) || (ret == WS_RESPONSE_TIMEOUT) || (retry == 0) ) {
		// we lost weather station, maybe temperature data can be saved
		store_to_database(db_name, tb_name, usr_name, usr_pw, in_temp_data, out_temp_data, -1, -1, 1, (const char *)err_msg);
		return -1;
	}
	
	if ( (ret != 1) || (rxbuf[0] == 0xff) ) {
		humi_data = -1;
		printf("humidity data error\n");
	} else {
		humi_data = rxbuf[0];
		printf("humidity %d\n", humi_data); 
	}
	
	/* read wind-speed sensor data, should be 4 bytes long */
	strcpy(txbuf, "w");
	retry = 3;
	do {
		ret = rf24_cmd(txbuf, rxbuf, strlen(txbuf), err_msg);
	} while ( (ret < 0) && (-- retry) );
	
	if ( (ret == WS_CONNECT_FAILED) || (ret == WS_RESPONSE_TIMEOUT) || (retry == 0) ) {
		// we lost weather station, maybe temperature and humidity data can be saved
		store_to_database(db_name, tb_name, usr_name, usr_pw, in_temp_data, out_temp_data, humi_data, -1, 1, (const char *)err_msg);
		return -1;
	}
	uint32_t wbuf;
	memcpy(&wbuf, rxbuf, 4);
	
	if ( (ret != 4) || (wbuf == 0xffffffff) ) {
		wind_data = -1;
		printf("wind velocity data error\n");
	} else {
		wind_data = wbuf;
		printf("wind velocity %d\n", wind_data);
	}
	
	if ( (in_temp_data == -1) || (humi_data == -1) || (wind_data == -1) ) {
		sprintf(err_msg, "Sensor(s) data error");
		store_to_database(db_name, tb_name, usr_name, usr_pw, in_temp_data, out_temp_data, humi_data, wind_data, 1, (const char *)err_msg);
	} else {
		store_to_database(db_name, tb_name, usr_name, usr_pw, in_temp_data, out_temp_data, humi_data, wind_data, 0, "");
	}
	
	return 0;

  // Setup and configure rf radio
  radio.begin();

  // optionally, increase the delay between retries & # of retries
  radio.setRetries(15, 15);
  // Dump the configuration of the rf unit for debugging
  //radio.printDetails();


/********* Role chooser ***********/

  printf("\n ************ Role Setup ***********\n");
  string input = "";
  char myChar = {0};
  cout << "Choose a role: Enter 0 for pong_back, 1 for ping_out (CTRL+C to exit) \n>";
  getline(cin,input);

  if(input.length() == 1) {
	myChar = input[0];
	if(myChar == '0'){
		cout << "Role: Pong Back, awaiting transmission " << endl << endl;
	}else{  
		cout << "Role: Ping Out, starting transmission " << endl << endl;
		role = role_ping_out;
	}
  }
/***********************************/
  // This simple sketch opens two pipes for these two nodes to communicate
  // back and forth.

    if ( !radioNumber )    {
      radio.openWritingPipe(pipes[0]);
      radio.openReadingPipe(1,pipes[1]);
    } else {
      radio.openWritingPipe(pipes[1]);
      radio.openReadingPipe(1,pipes[0]);
    }
	
	radio.startListening();
	
	// forever loop
	while (1)
	{
		if (role == role_ping_out)
		{
			// First, stop listening so we can talk.
			radio.stopListening();

			// Take the time, and send it.  This will block until complete

			printf("Now sending...\n");
			unsigned long time = millis();

			bool ok = radio.write( &time, sizeof(unsigned long) );

			if (!ok){
				printf("failed.\n");
			}
			// Now, continue listening
			radio.startListening();

			// Wait here until we get a response, or timeout (250ms)
			unsigned long started_waiting_at = millis();
			bool timeout = false;
			while ( ! radio.available() && ! timeout ) {
				if (millis() - started_waiting_at > 200 )
					timeout = true;
			}


			// Describe the results
			if ( timeout )
			{
				printf("Failed, response timed out.\n");
			}
			else
			{
				// Grab the response, compare, and send to debugging spew
				unsigned long got_time;
				radio.read( &got_time, sizeof(unsigned long) );

				// Spew it
				printf("Got response %lu, round-trip delay: %lu\n",got_time,millis()-got_time);
			}
			sleep(1);
		}

		//
		// Pong back role.  Receive each packet, dump it out, and send it back
		//

		if ( role == role_pong_back )
		{
			
			// if there is data ready
			if ( radio.available() )
			{
				// Dump the payloads until we've gotten everything
				unsigned long got_time;

				// Fetch the payload, and see if this was the last one.
				while(radio.available()){
					radio.read( &got_time, sizeof(unsigned long) );
				}
				radio.stopListening();
				
				radio.write( &got_time, sizeof(unsigned long) );

				// Now, resume listening so we catch the next packets.
				radio.startListening();

				// Spew it
				printf("Got payload(%d) %lu...\n",sizeof(unsigned long), got_time);
				
				delay(925); //Delay after payload responded to, minimize RPi CPU time
				
			}
		
		}

	} // forever loop

  return 0;
}

void rf24_lowlevel_init(void) {
	radio.begin();
	radio.setAutoAck(1);
	radio.setRetries(15, 15);
	radio.setPALevel(RF24_PA_MAX);
	radio.setCRCLength(RF24_CRC_8);
	radio.setDataRate(RF24_250KBPS);
	radio.enableDynamicPayloads();
	radio.openWritingPipe(pipes[1]);
	radio.openReadingPipe(1, pipes[0]);
}

int rf24_read(char *buf, unsigned long timeout_ms) {
	radio.startListening();

	// Wait here until we get a response, or timeout
	unsigned long started_waiting_at = millis();
	bool timeout = false;
	
	while ( ! radio.available() && ! timeout ) {
		if ((millis() - started_waiting_at) > timeout_ms) {
			timeout = true;
		}
	}
	
	if ( timeout ) {
		//printf("response timed out\n");
		return -1;
	} else {
		int len = radio.getDynamicPayloadSize();
		//printf("incoming %d bytes\n", len);
		radio.read(buf, len);
		//buf[len] = '\0';
		//printf("%s", buf);
		return len;
	}
}

int rf24_write(char *buf, int len) {
	radio.stopListening();
	bool ok = radio.write(buf, len);
	if (!ok){
		return -1;
	}
	//printf("output %d bytes\n", len);
	return 0;
}

int rf24_cmd(char *txbuf, char *rxbuf, int txlen, char *err) {
	int count;
	const char err_msg_0[] = "Connect to weather station failed.\n";
	const char err_msg_1[] = "Weather station response time out.\n";
	if (rf24_write(txbuf, txlen) != 0) {
		//printf("write failed\n");
		fprintf(stderr, err_msg_0);
		sprintf(err, err_msg_0);
		return WS_CONNECT_FAILED;
	}
	
	count = rf24_read(rxbuf, 100);
	if (count == -1) {
		sprintf(err, err_msg_1);
		return WS_RESPONSE_TIMEOUT;
	}
	sprintf(err, "%s", "");
	return count;
}

#define finish_with_error(_con_) \
  fprintf(stderr, "%s\n", mysql_error(_con_));\
  mysql_close(_con_);\
  return -1;

int store_to_database(const char *db_name, const char *tb_name, const char *user_name, const char *user_pw, 
	int indoor_temp_data, int outdoor_temp_data, int humi_data, int wind_data, int err, const char *err_msg) {
	MYSQL *con;
	char str[1024];
	
	//printf("MySQL client version: %s\n", mysql_get_client_info());	
	con = mysql_init(NULL);
	
	if (con == NULL) {
		finish_with_error(con);
	}
	
	if (mysql_real_connect(con, "localhost", user_name, user_pw, db_name, 0, NULL, 0) == NULL) {
		// try to create the database
		if (mysql_real_connect(con, "localhost", user_name, user_pw, NULL, 0, NULL, 0) == NULL) { 
			finish_with_error(con);
		} else {
			sprintf(str, "CREATE DATABASE %s", db_name);
			if (mysql_query(con, str)) {
				finish_with_error(con);
			} else {
				printf("no database '%s', create a new one\n", db_name);
				mysql_close(con);
				con = mysql_init(NULL);
				if (con == NULL) {
					finish_with_error(con);
				}				
				// re-connect the database
				if (mysql_real_connect(con, "localhost", user_name, user_pw, db_name, 0, NULL, 0) == NULL) {
					finish_with_error(con);
				}
			}
			// try to create the new data table
			sprintf(str, "CREATE TABLE %s(Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, TemperatureIndoor INT, TemperatureOutdoor INT, Humidity INT, Windspeed INT, Error INT, ErrorMessage TEXT, Ctime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)", tb_name);
			if (mysql_query(con, str)) {      
				finish_with_error(con);
			} else {
				printf("no table '%s', create a new one\n", tb_name);
			}
		}
	} 
	
	sprintf(str, "INSERT INTO %s (TemperatureIndoor, TemperatureOutdoor, Humidity, Windspeed, Error, ErrorMessage, Ctime) VALUES(%d,%d,%d,%d,%d,'%s',CURRENT_TIMESTAMP)", tb_name, indoor_temp_data, outdoor_temp_data, humi_data, wind_data, err, err_msg);
	if (mysql_query(con, str)) {
		finish_with_error(con);
	} else {
		printf("insert to table '%s' in database '%s'\n", tb_name, db_name);
	}
	
	mysql_close(con);
	return 0;
}

int delete_database(const char *db_name) {
	MYSQL *con = mysql_init(NULL);
	char str[128];
	
	if (con == NULL) {
		finish_with_error(con);
	} 
	 
	if (mysql_real_connect(con, "localhost", "root", "198579", NULL, 0, NULL, 0) == NULL) { 
		finish_with_error(con);
	}    

	sprintf(str, "DROP DATABASE %s", db_name);
	if (mysql_query(con, str)) {
		finish_with_error(con);
	} else {
		printf("delete database '%s'", db_name);
	}
	mysql_close(con);
	return 0;
}

int read_indoor_temperature(void) {
	DIR *dir;
	struct dirent *dirent;
	char dev[16];      // Dev ID
	char devPath[128]; // Path to device
	char buf[256];     // Data from device
	char tmpData[6];   // Temp C * 1000 reported by device 
	char path[] = "/sys/bus/w1/devices"; 
	ssize_t numRead;
	int temp;
 
	dir = opendir (path);
	if (dir != NULL) {
		while ((dirent = readdir (dir)))
		// 1-wire devices are links beginning with 28-
		if (dirent->d_type == DT_LNK && strstr(dirent->d_name, "28-") != NULL) { 
			strcpy(dev, dirent->d_name);
			// printf("\nDevice: %s\n", dev);
		}
		closedir (dir);
	} else {
		perror ("Couldn't open the w1 devices directory");
		return -1;
	}

    // Assemble path to OneWire device
	sprintf(devPath, "%s/%s/w1_slave", path, dev);
	// Read temp continuously
	// Opening the device's file triggers new reading
	//while(1) {
	int fd = open(devPath, O_RDONLY);
	if(fd == -1) {
		perror ("Couldn't open the w1 device.");
		return -1;   
	}
	
	int retry = 10;
	do {
		numRead = read(fd, buf, 256);
		if (numRead > 0) {
			buf[numRead] = '\0';
			uint8_t lsb, msb;
			//printf("buf='%s'\n", buf);
			if (strstr(buf,"YES")) {
				sscanf(buf, "%x %x", &lsb, &msb);
				//printf("0x%x, 0x%x\n", lsb, msb);
				temp = (msb << 8) | lsb;
				break;
			}
			//strncpy(tmpData, strstr(buf, "t=") + 2, 5); 
			//float tempC = strtof(tmpData, NULL) / 1000;
			//temp = tempC * 16; // keep the
			//printf("Device: %s  - ", dev); 
			//printf("indoor temperature: %.2f", tempC / 1000);
			//printf("%.3f F\n\n", (tempC / 1000) * 9 / 5 + 32);
		}
	} while (-- retry);	
	close(fd);
	if (retry == 0) {
		return -1;
	} else {
		//printf("indoor temperature %.1f\n", (float)temp / 16.0);
		return temp;
	}
}
