/*
 Copyright (C) 2011 J. Coliz <maniacbug@ymail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.

 03/17/2013 : Charles-Henri Hallard (http://hallard.me)
              Modified to use with Arduipi board http://hallard.me/arduipi
						  Changed to use modified bcm2835 and RF24 library
TMRh20 2014 - Updated to work with optimized RF24 Arduino library

 */

/**
 * Example RF Radio Ping Pair
 *
 * This is an example of how to use the RF24 class on RPi, communicating to an Arduino running
 * the GettingStarted sketch.
 */
#include <stdio.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <climits>
#include <RF24/RF24.h>
#include <mysql/mysql.h>

using namespace std;

#define WS_CONNECT_FAILED		-1
#define WS_RESPONSE_TIMEOUT		-2
#define MWS_MAX_NODES			32

#define finish_with_error(_con_) \
  fprintf(stderr, "%s\n", mysql_error(_con_));\
  mysql_close(_con_);\
  return -1;

// RPi generic:
RF24 radio(22, 0);

// database keycard
typedef struct {
	char host[32];
	char name[32];
	char table[32];
	char usr[32];
	char psw[32];
} db_id_t;

// message from Arduino mws-node
typedef struct {
	int mws_id;
	int temperature;
	int humidity;
	int wind_speed;
	int rainy_code;
} mws_node_data_t;

// what we read in radio packet
typedef struct {
  uint16_t humidity;
  uint16_t temperature;
  uint16_t windspeed;
  uint16_t raincode;
} mws_radio_packet_t;

// mws-node keycard
typedef struct {
	int rfch;
	uint64_t addr;
	char name[32];
	mws_node_data_t data;
} mws_node_t;

typedef struct {
	const char *option;
	const char *details;
} prog_opts_t;

int rf24_write(char *buf, int len);
int rf24_read(char *buf, unsigned long timeout_ms);
void rf24_init(RF24 *radio, int rfch, uint64_t addr);
int rf24_cmd(char *txbuf, char *rxbuf, int txlen, char *err);
int reset_node_sampling_period(uint32_t sec);

int read_indoor_temperature(void);
int delete_database(const char *db_name);
int call_the_mws_node(mws_node_t *node, char *err_msg);
int store_to_database(db_id_t *db, mws_node_t *mws, int mws_id, int err, const char *err_msg);
void print_help_msg(void);
int print_error_and_help(const char *err, int ret);

static mws_node_t mws_nodes[MWS_MAX_NODES];
static int mws_nodes_count = 0;

const prog_opts_t opts[] = {
	{"-help",  	"Display this information"},
	{"-host",	"Mysql database host, default is 'localhost'"},
	{"-db", 	"Mysql database name, default is 'MWSDatabase'"},
	{"-tl", 	"Mysql database table name, default is 'MWSHomeData'"},
	{"-u",		"Mysql user name, default is 'root'"},
	{"-p",		"Mysql user password, default is empty"},
	{"-n",		"Arduino mws node, with channel, address and name"},
	{"-d",		"Delete the database"},
	{"-s",		"Arduino mws node sampling period, for second"},
	{"-t",		"Test only, no write to database"},
	{NULL, NULL},
};

db_id_t mws_db = {
	"localhost",
	"MWSDatabase",
	"MWSHomeData",
	"root",
	"",
};

int main(int argc, char** argv) {
	char err_msg[128];
	bool remove_database = false;
	bool set_sp = false;
	int sp_sec = -1;
	bool test_only = false;
	
	for(int i = 1; i < argc; i ++) {
		if (argv[i][0] == '-') {
			if (strcmp(&argv[i][1], "db") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						return print_error_and_help("no database name input", -1);
					} else { 
						sprintf(mws_db.name, "%s", argv[i+1]);
					}
				}
			} else if (strcmp(&argv[i][1], "host") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						return print_error_and_help("no database host input", -1);						
					} else { 
						sprintf(mws_db.host, "%s", argv[i+1]);
					}
				}
			} else if (strcmp(&argv[i][1], "tl") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						return print_error_and_help("no table name input", -1);
					} else { 
						sprintf(mws_db.table, "%s", argv[i + 1]);
					}
				}			
			} else if (strcmp(&argv[i][1], "d") == 0) { 		 
				remove_database = true;
			} else if (strcmp(&argv[i][1], "u") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						return print_error_and_help("no database user name input", -1);
					} else { 
						sprintf(mws_db.usr, "%s", argv[i + 1]);
					}
				}
			} else if (strcmp(&argv[i][1], "p") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						return print_error_and_help("no database user password input", -1);
					} else { 
						sprintf(mws_db.psw, "%s", argv[i + 1]);
					}
				}
			} else if (strcmp(&argv[i][1], "n") == 0) {
				if (argc > (i + 3)) {
					int ch;
					uint64_t addr;
					char name[32];
					if (sscanf(argv[i + 1], "%d", &ch) != 1) {
						return print_error_and_help("unrecognized mws channel", -1);
					}
					if (ch < 0 || ch > 127) {
						return print_error_and_help("mws channel must between 0 to 127", -1);
					}
					
					char *end;
					errno = 0;
					addr = strtoull(argv[i + 2], &end, 16);
					if ( (addr == 0 && end == argv[i + 2]) || 
					     (addr == ULLONG_MAX && errno) ) {
						return print_error_and_help("unrecognized mws address", -1);
					} else {
						addr &= 0xffffffffff;
					}
					if (argv[i + 3][0] == '-') {
						sprintf(name, "%s", "");
					} else {
						if (sscanf(argv[i + 3], "%s", name) != 1) {
							return print_error_and_help("unrecognized mws name", -1);
						}	
					}			
					//printf("%d-%llx-%s\n", ch, addr, name);	
					mws_nodes[mws_nodes_count].rfch = ch;
					mws_nodes[mws_nodes_count].addr = addr;
					strcpy(mws_nodes[mws_nodes_count].name, name);
					mws_nodes_count ++;
					
					if (mws_nodes_count >= MWS_MAX_NODES) {
						return print_error_and_help("too many nodes input, %d nodes maxim", -1);						
					}
				}
			} else if (strcmp(&argv[i][1], "s") == 0) {
				if (argc > (i + 1)) {
					if (argv[i + 1][0] == '-') {
						return print_error_and_help("no sampling period input(seconds)", -1);
					} else { 
						if (sscanf(argv[i + 1], "%d", &sp_sec) != 1) {
							return print_error_and_help("sampling period error", -1);
						} 
						if (sp_sec < 5 && sp_sec > (24 * 3600)) {
							return print_error_and_help("sampling period must between 5 to (24 * 3600)", -1);
						} 
						set_sp = true;
					}
				}
			} else if (strcmp(&argv[i][1], "help") == 0) {
				print_help_msg();
				return 0;
			} else if (strcmp(&argv[i][1], "t") == 0) {
				test_only = true;
			} else {
				printf("error: unrecognized command line option '%s'\n", &argv[i][1]);
				printf("use -help to display all options\n");
				return -1;
			} 
		}
	}
	
	if (mws_nodes_count == 0) {
		printf("no input mws's node\n");
		return 0;
	}
	printf("Database configuratio:\n");
	printf("host : '%s'\n", mws_db.host);
	printf("name : '%s'\n", mws_db.name);
	printf("table: '%s'\n", mws_db.table);
	printf("user : '%s'\n", mws_db.usr);
	printf("psw  : '%s'\n", mws_db.psw);
	 
	if (remove_database) {
		delete_database(mws_db.name);
		return 0;
	}
	
	//rf24_init(&radio, 100, 0xE7E7E7E7E7LL);
	for (int i = 0; i < mws_nodes_count; i ++) {
		printf("---------------- mws-%d-%llx-%s ----------------\n", mws_nodes[i].rfch, mws_nodes[i].addr, mws_nodes[i].name);
		rf24_init(&radio, mws_nodes[i].rfch, mws_nodes[i].addr);
		
		if (set_sp) {
			printf("Reset mws node sampling period to %d seconds ", sp_sec);
			if (reset_node_sampling_period(sp_sec) < 0) {
				printf("connect to mws node failed\n");
			} else {
				printf("success\n");
			}
		} else {
			if (call_the_mws_node(&mws_nodes[i], err_msg) == 0) {
				printf("temperature %0.1f, humidity %d, wind speed %d, rainy code %d\n", 
					(float)mws_nodes[i].data.temperature / 16.0, mws_nodes[i].data.humidity, mws_nodes[i].data.wind_speed, mws_nodes[i].data.rainy_code);
				if (test_only == false) {
					store_to_database(&mws_db, &mws_nodes[i], i, 0, "");
				} else {
					printf("test only, no write to database\n");
				}
			} else {
				if (test_only == false) {
					store_to_database(&mws_db, &mws_nodes[i], i, 1, err_msg);
				} else {
					printf("test only, no write to database\n");
				}
			}
		}
		printf("\n");
	}
	return 0;
}

void rf24_init(RF24 *radio, int rfch, uint64_t addr) {
	radio->begin();
	radio->setChannel(rfch);
	radio->setAutoAck(1);
	radio->setRetries(15, 15);
	radio->setPALevel(RF24_PA_MAX);
	radio->setCRCLength(RF24_CRC_8);
	radio->setDataRate(RF24_250KBPS);
	radio->enableDynamicPayloads();
	radio->openWritingPipe(addr);
	radio->openReadingPipe(1, 0xE7E7E7E7E7LL);
}

int rf24_read(char *buf, unsigned long timeout_ms) {
	radio.startListening();

	// Wait here until we get a response, or timeout
	unsigned long started_waiting_at = millis();
	bool timeout = false;
	
	while ( ! radio.available() && ! timeout ) {
		if ((millis() - started_waiting_at) > timeout_ms) {
			timeout = true;
		}
	}
	
	if ( timeout ) {
		//printf("response timed out\n");
		return -1;
	} else {
		int len = radio.getDynamicPayloadSize();
		//printf("incoming %d bytes\n", len);
		radio.read(buf, len);
		//buf[len] = '\0';
		//printf("%s", buf);
		return len;
	}
}

int rf24_write(char *buf, int len) {
	radio.stopListening();
	if (radio.write(buf, len) == false){
		return -1;
	}
	//printf("output %d bytes\n", len);
	return 0;
}

int rf24_cmd(char *txbuf, char *rxbuf, int txlen, char *err) {
	int count;
	const char err_msg_0[] = "Connect to weather station failed";
	const char err_msg_1[] = "Weather station response time out";
	
	if (rf24_write(txbuf, txlen) != 0) {
		//printf("write failed\n");
		//fprintf(stderr, err_msg_0);
		sprintf(err, err_msg_0);
		return WS_CONNECT_FAILED;
	}
	
	count = rf24_read(rxbuf, 500);
	if (count == -1) {
		sprintf(err, err_msg_1);
		return WS_RESPONSE_TIMEOUT;
	}
	sprintf(err, "%s", "");
	return count;
}

int reset_node_sampling_period(uint32_t sec) {
	char buf[6] = "sp";
	int retry = 10;
	int ret;
	
	memcpy(&buf[2], &sec, sizeof(uint32_t));
	do {
		ret = rf24_write(buf, 6);
		if (ret != 0) {
			printf(".");
		}
	} while (ret != 0 && --retry);
	
	if (retry == 0 || ret != 0) {
		return WS_CONNECT_FAILED;
	}	
	return 0;
}

int print_error_and_help(const char *err, int ret) {
	printf("error: %s\n", err);
	printf("use -help to display all options\n");
	return ret;
}

void print_help_msg(void) {

	printf("Usage: mws [options] -node [channel][address][name] -node ... \r\n");
	printf("Options: \r\n");
	
	for(int i = 0; ; i ++) {
		if (opts[i].option) {
			printf("  %-16s", opts[i].option);
			printf("%s\n", opts[i].details);
		} else {
			break;
		}
	}
	printf("\nExamples:\n");
	printf("  sudo mws -u root -p 198579 -node 99 bfd3856cbb living-room -node 99 d0dec4c59e study-room\n");
	printf("  sudo mws -db test -tl test_table -u root -p 198579 -node 99 bfd3856cbb outdoor\n");
	printf("  sudo mws -s 900 -node 99 bfd3856cbb test\n");
}

int call_the_mws_node(mws_node_t *node, char *err_msg) {
	char rxbuf[32];
	char txbuf[] = "hi";
	int ret, retry;
	
	//rf24_init(&radio, rfch, addr);
	radio.stopListening();
	radio.setChannel(node->rfch);
	radio.openWritingPipe(node->addr);
	//radio.printDetails();
	radio.startListening();
	delay(100);
	radio.stopListening();
	
	retry = 10;
	do {
		ret = rf24_cmd(txbuf, rxbuf, strlen(txbuf), err_msg);
		if (ret < 0) {
			printf("%s, retry\n", err_msg);
			//delay(5000);
		}
	} while ( (ret < 0) && (-- retry) );
	
	if ( (ret == WS_CONNECT_FAILED) || (ret == WS_RESPONSE_TIMEOUT) || 
		 (ret != sizeof(mws_radio_packet_t)) || (retry == 0) ) {
		node->data.temperature = -1;
		node->data.humidity = -1;
		node->data.wind_speed = -1;
		node->data.rainy_code = -1;
		sprintf(err_msg, "Radio packet error");
		return -1;
	} else { 
		// to satisfied the compiler
		//uint16_t tbuf, wbuf;
		//memcpy(&tbuf, &rxbuf[0], sizeof(uint16_t));
		//memcpy(&wbuf, &rxbuf[2], sizeof(uint16_t));
		//result->temperature = *(uint16_t *)&rxbuf[0];
		//result->wind_speed  = *(uint16_t *)&rxbuf[2];
		//node->data.temperature = tbuf;
		//node->data.wind_speed = wbuf;
		//node->data.humidity = rxbuf[4];		
		//node->data.rainy_code = rxbuf[5];
		mws_radio_packet_t buf;
		memcpy(&buf, rxbuf, sizeof(mws_radio_packet_t));
		node->data.temperature = buf.temperature;
		node->data.wind_speed = buf.windspeed;
		node->data.humidity = buf.humidity;		
		node->data.rainy_code = buf.raincode;
		
		if ( node->data.temperature == 0xff || node->data.wind_speed == 0xff ||
			 node->data.humidity == 0xff || node->data.rainy_code == 0xff ) {
			sprintf(err_msg, "Sensor(s) data error");
			return -1;
		}
		return 0;
	}
}

int store_to_database(db_id_t *db, mws_node_t *node, int node_id, int err, const char *err_msg) {
	MYSQL *con;
	char str[1024];
	
	//printf("MySQL client version: %s\n", mysql_get_client_info());	
	con = mysql_init(NULL);
	
	if (con == NULL) {
		finish_with_error(con);
	} 
	
	if (mysql_real_connect(con, db->host, db->usr, db->psw, db->name, 0, NULL, 0) == NULL) {
		// try to create the database
		if (mysql_real_connect(con, db->host, db->usr, db->psw, NULL, 0, NULL, 0) == NULL) { 
			finish_with_error(con);
		} else {
			sprintf(str, "CREATE DATABASE %s", db->name);
			if (mysql_query(con, str)) {
				finish_with_error(con);
			} else {
				printf("no database '%s', create a new one\n", db->name);
				mysql_close(con);
				con = mysql_init(NULL);
				if (con == NULL) {
					finish_with_error(con);
				}				
				// re-connect the database
				if (mysql_real_connect(con, db->host, db->usr, db->psw, db->name, 0, NULL, 0) == NULL) {
					finish_with_error(con);
				}
			}
			// try to create the new data table
			sprintf(str, "CREATE TABLE %s(Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, MWSId INT, MWSName VARCHAR(32), Temperature INT, Humidity INT, Windspeed INT, Rainycode INT, Error INT, ErrorMessage TEXT, Ctime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)", db->table);
			if (mysql_query(con, str)) {      
				finish_with_error(con);
			} else {
				printf("no table '%s', create a new one\n", db->table);
			}
		}
	} 
	
	sprintf(str, "INSERT INTO %s (MWSId, MWSName, Temperature, Humidity, Windspeed, Rainycode, Error, ErrorMessage, Ctime) VALUES(%d,'%s',%d,%d,%d,%d,%d,'%s',CURRENT_TIMESTAMP)", 
		db->table, node_id, node->name, node->data.temperature, node->data.humidity, node->data.wind_speed, node->data.rainy_code, err, err_msg);
	//printf("query:\n%s\n", str);
	
	if (mysql_query(con, str)) {
		finish_with_error(con);
	} else {
		printf("insert to table '%s' in database '%s'\n", db->table, db->name);
	}
	
	mysql_close(con);
	return 0;
}

int delete_database(const char *db_name) {
	MYSQL *con = mysql_init(NULL);
	char str[128];
	
	if (con == NULL) {
		finish_with_error(con);
	} 
	 
	if (mysql_real_connect(con, "localhost", "root", "198579", NULL, 0, NULL, 0) == NULL) { 
		finish_with_error(con);
	}    

	sprintf(str, "DROP DATABASE %s", db_name);
	if (mysql_query(con, str)) {
		finish_with_error(con);
	} else {
		printf("delete database '%s'", db_name);
	}
	mysql_close(con);
	return 0;
}

// use wiringPi to read ds18b20 which connect to raspi
int read_indoor_temperature(void) {
	DIR *dir;
	struct dirent *dirent;
	char dev[16];      // Dev ID
	char devPath[128]; // Path to device
	char buf[256];     // Data from device
	//char tmpData[6];   // Temp C * 1000 reported by device 
	char path[] = "/sys/bus/w1/devices"; 
	ssize_t numRead;
	int temp;
 
	dir = opendir (path);
	if (dir != NULL) {
		while ((dirent = readdir (dir)))
		// 1-wire devices are links beginning with 28-
		if (dirent->d_type == DT_LNK && strstr(dirent->d_name, "28-") != NULL) { 
			strcpy(dev, dirent->d_name);
			// printf("\nDevice: %s\n", dev);
		}
		closedir (dir);
	} else {
		perror ("Couldn't open the w1 devices directory");
		return -1;
	}

    // Assemble path to OneWire device
	sprintf(devPath, "%s/%s/w1_slave", path, dev);
	// Read temp continuously
	// Opening the device's file triggers new reading
	//while(1) {
	int fd = open(devPath, O_RDONLY);
	if(fd == -1) {
		perror ("Couldn't open the w1 device.");
		return -1;   
	}
	
	int retry = 10;
	do {
		numRead = read(fd, buf, 256);
		if (numRead > 0) {
			buf[numRead] = '\0';
			unsigned int lsb, msb;
			//printf("buf='%s'\n", buf);
			if (strstr(buf,"YES")) {
				sscanf(buf, "%x %x", &lsb, &msb);
				//printf("0x%x, 0x%x\n", lsb, msb);
				temp = (msb << 8) | lsb;
				break;
			}
			//strncpy(tmpData, strstr(buf, "t=") + 2, 5); 
			//float tempC = strtof(tmpData, NULL) / 1000;
			//temp = tempC * 16; // keep the
			//printf("Device: %s  - ", dev); 
			//printf("indoor temperature: %.2f", tempC / 1000);
			//printf("%.3f F\n\n", (tempC / 1000) * 9 / 5 + 32);
		}
	} while (-- retry);	
	close(fd);
	if (retry == 0) {
		return -1;
	} else {
		//printf("indoor temperature %.1f\n", (float)temp / 16.0);
		return temp;
	}
}
