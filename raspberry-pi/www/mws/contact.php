<!DOCTYPE html>
<html lang="zh-CN">
  <head>    
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<!--<meta http-equiv="refresh" content="900">-->
    <title>mini weather station</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<!--<link rel="stylesheet" href="weather-icons/css/weather-icons.css" type="text/css" media="all" />-->
    <link rel="stylesheet" href="css/style2.css" >
		
		<!--<script type="text/javascript" src="fusioncharts/js/fusioncharts.js"></script>-->
		<!--<script type="text/javascript" src="fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>-->    
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Mini Weather Station</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Data</a></li>
            <li><a href="about.php">About</a></li>
            <li class="active"><a href="contact.php">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container starter" role="main"></div>
		
		<div class="container">
			<h1>alivehex@gmail.com</h1>
			<hr>
			<footer>
				<p>&copy; 2016 alivehex@gmail.com</p>
			</footer>
		</div><!-- /.container --> 
		
		
		
					
					
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
  </body>
</html>