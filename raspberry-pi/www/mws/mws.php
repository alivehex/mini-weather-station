<?php
require_once('global_values.php');


function query_database($str) {
    $con = mysql_connect(DB_ADDR, DB_USR, DB_PSW);

    if (!$con) {
        error('connect to database failed');
        return false;
    } 

    if (!mysql_select_db(DB_NAME, $con)) {
        error('select database "'.DB_NAME.'" failed');
        mysql_close($con);
        return false;
    } 

    $result = mysql_query($str);
    mysql_close($con);
    return $result;
} 

function min_max_value($array) {
    $min = false;
    $max = false;         

    foreach($array as $key=>$item) {
        if (($min === false) || ($item['value'] <= $min['value'])) {
            $min['value'] = $item['value'];
            $min['index'] = $key;
        }
        if (($max === false) || ($item['value'] >= $max['value'])) {
            $max['value'] = $item['value'];
            $max['index'] = $key;
        }
        //$index ++;
    }
    $result = array('min'=>$min, 'max'=>$max);
    return $result;
}

function fresh_data($node_name) {
    $start_time = date("Y-m-d 00:00:00");
    $end_time = date("Y-m-d 00:00:00", time() + (24 * 3600));

    $query = "SELECT * FROM ".TBL_NAME." WHERE MWSName='$node_name' AND Error='0' AND Ctime>='$start_time' AND Ctime<='$end_time' ORDER BY Ctime";
    //echo $query.'</br>';
    $result = query_database($query);

    $data_array['t'] = array();
    $data_array['w'] = array();
    $data_array['h'] = array();
    $data_array['r'] = array();

    while (1) {
				$row = mysql_fetch_array($result);
        if ($row !== false) {
            $item['label'] = date("G:i", strtotime($row['Ctime']));
            $item['date'] = date("Y-m-d G:i", strtotime($row['Ctime']));
            $item['value'] = round($row['Temperature'] / 16, 0);  
            array_push($data_array['t'], $item);

            $item['value'] = round($row['Windspeed'] / 15, 0);
            array_push($data_array['w'], $item);

            $item['value'] = $row['Humidity'];
            array_push($data_array['h'], $item); 
            
            $item['value'] = $row['Rainycode'];
            array_push($data_array['r'], $item);
        } else {
            break;
        }
    }
    return $data_array;
} 

function mark_min_max_with_vline(&$array, &$mark, $min_label, $max_label) {
    $vline[0]["vline"]="true";
    $vline[0]["label"]=$min_label; 
    $vline[0]["linePosition"]="1";
		$vline[0]["dashed"]="1";
    $vline[0]["dashLen"]="4";
    $vline[0]["dashGap"]= "2";
		$vline[0]["color"]= "#BFBFBF";
    $vline[0]["thickness"]= "2";
		$vline[0]["showLabelBorder"] = "0";
		$vline[0]["labelBgColor"] = "0";
		
    array_splice($array, $mark['min']['index'], 0,  $vline);

    //$max_vline[0]["vline"]="true";
    $vline[0]["label"]=$max_label;
    // 增加的$min_vline现在也占一个元素
    array_splice($array, $mark['max']['index'] + 1, 0,  $vline);
}

?>