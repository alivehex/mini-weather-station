<!DOCTYPE html>
<html lang="zh-CN">
  <head>    
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="refresh" content="900">
    <title>mini weather station</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="weather-icons/css/weather-icons.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/style.css" >
		
		<script type="text/javascript" src="fusioncharts/js/fusioncharts.js"></script>
		<script type="text/javascript" src="fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>
		
		<!-- scripts for aqicn pm2.5 display -->
    <script  type="text/javascript"  charset="utf-8">  
        (function(w,d,t,f){  w[f]=w[f]||function(c,k,n){s=w[f],k=s['k']=(s['k']||(k?('&k='+k):''));s['c']=  
        c=(c  instanceof  Array)?c:[c];s['n']=n=n||0;L=d.createElement(t),e=d.getElementsByTagName(t)[0];  
        L.async=1;L.src='//feed.aqicn.org/feed/'+(c[n].city)+'/'+(c[n].lang||'')+'/feed.v1.js?n='+n+k;  
        e.parentNode.insertBefore(L,e);  };  })(  window,document,'script','_aqiFeed'  );    
    </script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Mini Weather Station</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Data</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
		<?php
			require_once './mws.php';

			$ourdoor_data_array = fresh_data("outdoor");
			$indoor_data_array = fresh_data("bedroom");
			$outdoor_index = count($ourdoor_data_array['t']) - 1;
			$indoor_index = count($indoor_data_array['t']) - 1;
			$update_time = $ourdoor_data_array['t'][$outdoor_index]['date'];
		?> 

    <div class="container starter" role="main"></div>
		
		<div class="container">
			<div class="row">					
				<!--<div class="col-md-2"></div>-->
				<div class="col-md-12">
					<table class="table table-striped" id="weather-table">
						<!-- PM2.5 -->
						<tr>
							<td class="w-20"><i class="wi wi-dust big-icon colored-icon"></i></td>
							<td class="w-80" colspan="6"><span class="big-text" id="city-aqi-container-display"></span></td>
						</tr>

						<!-- 温度 -->
						<tr>
							<td class="w-20"><i class="wi wi-thermometer big-icon colored-icon"></i></td>
							<td class="w-10" rowspan="2"><span class="label label-primary middle-text">室内</span></td>
							<td class="w-15 sensor-value"><span class="big-text"><?php echo $indoor_data_array['t'][$indoor_index]['value']?></span></td>
							<td class="w-15 unit"><span class="big-text">&#176</span></td>
							
							<td class="w-10" rowspan="2"><span class="label label-success middle-text">室外</span></td>
							<td class="w-15 sensor-value"><span class="big-text"><?php echo $ourdoor_data_array['t'][$outdoor_index]['value']?></span></td>
							<td class="w-15 unit"><span class="big-text">&#176</span></td>
						</tr>
						
						<!-- 湿度 -->
						<tr>
							<td class="w-20"><i class="wi wi-humidity big-icon colored-icon"></i></td>							
							<td class="w-15 sensor-value"><span class="big-text"><?php echo $indoor_data_array['h'][$indoor_index]['value']?></span></td>
							<td class="w-15 unit"><span class="big-text">%</span></td>
							<td class="w-15 sensor-value"><span class="big-text"><?php echo $ourdoor_data_array['h'][$outdoor_index]['value']?></span></td>
							<td class="w-15 unit"><span class="big-text">%</span></td>
						</tr>
						
						<!-- 风速 -->
						<tr>
							<td class="w-20"><i class="wi wi-tornado big-icon colored-icon"></i></td>
							<td class="w-80" colspan="6">
								<span class="big-text"><?php echo $ourdoor_data_array['w'][$outdoor_index]['value'].' RPM'; ?></span>
							</td>
						</tr>
						
						<!-- 雨水 -->
						<tr>
							<td class="w-20"><i class="wi wi-umbrella big-icon colored-icon"></i></td>
							<td class="w-80" colspan="6">
								<?php
									if ($ourdoor_data_array['r'][$outdoor_index]['value'] < 100) {
										echo '<i class="wi wi-alien big-icon"></i>';
									} else {
										echo '<i class="wi wi-rain big-icon"></i>';
									}
								?>
							</td>
						</tr>
						
						<!-- 温度图表 -->
						<tr>
							<td colspan="7"><div id="temperature-chart-container"></div></td>
						</tr>
						
						<!-- 风速图表 -->
						<tr>
							<td colspan="7"><div id="windspeed-chart-container"></div></td>
						</tr>
					</table>
					
					<!--<table class="table table-striped" id="chart-table">
						<tr>
							<td class="w-10" rowspan="2"><span class="label label-success middle-text">室外</span></td>
							<td class="w-90"><div id="temperature-chart-container"></div></td>
							
						</tr>
							<td class="w-90"><div id="windspeed-chart-container"></div></td>
						<tr>
							
						</tr>
					</table>-->
				</div>
				<!--<div class="col-md-2"></div>-->
			</div>
			
			<p>Last update <?php echo $update_time ?></p>
			<p>空气质量信息来源于aqicn.org</p>
			<hr>
			<footer>
				<p>&copy; 2016 alivehex@gmail.com</p>
			</footer>
		</div><!-- /.container --> 
		
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
		
		<script  type="text/javascript"  charset="utf-8">  
        _aqiFeed({ display:"%aqi <small>(%impact)</small>",container:"city-aqi-container-display",city:"xiamen",lang:"cn"});   
    </script>
        
		<?php
			$tmark = min_max_value($ourdoor_data_array['t']);
			mark_min_max_with_vline($ourdoor_data_array['t'], $tmark, $tmark['min']['value'].'℃', $tmark['max']['value'].'℃');

			$tmark2 = min_max_value($ourdoor_data_array['w']);
			mark_min_max_with_vline($ourdoor_data_array['w'], $tmark2, $tmark2['min']['value'], $tmark2['max']['value']);
		?>
    <script  type="text/javascript"  charset="utf-8"> 
        FusionCharts.ready(function() {
            var outdoor_t_chart = new FusionCharts({
                "type": "column2d",
                "renderAt": "temperature-chart-container",
                "width": "100%",
                "height": "140px",
                "dataFormat": "json",
                "dataSource": {
                    "chart": {
                        //"caption": "<?php //echo date("Y-m-d") ?>",                        
                        "showValues": "0",
                        "theme": "fint",
                        //"xAxisName": "",
                        "yAxisName": "温度(℃)",
                        "yAxisMinValue":"10<?php //echo $tmark['min']['value']?>", 
                        "yAxisMaxValue":"40<?php //echo $tmark['max']['value'] + 15?>",
												"canvasBgColor": "#000000",
												"canvasBgAlpha": "100",
												"bgColor": "#000000",
												"bgAlpha": "100",
												"showBorder": "0",
												"baseFontColor": "#7F7F7F",
                    },                            
                    "data": <?php echo json_encode($ourdoor_data_array['t']) ?>
                }
            });
            outdoor_t_chart.render();
						
						var outdoor_w_chart = new FusionCharts({
                "type": "line",
                "renderAt": "windspeed-chart-container",
                "width": "100%",
                "height": "140px",
                "dataFormat": "json",                    
                "dataSource": {
                    "chart": {
                        //"caption": "<?php //echo date("Y-m-d") ?>",                        
                        "showValues": "0",
                        "palettecolors": "#6baa01",
                        "anchorRadius": "0",
                        "theme": "fint",
												"canvasBgColor": "#000000",
												"canvasBgAlpha": "100",
												"bgColor": "#000000",
												"bgAlpha": "100",
												"showBorder": "0",
												"baseFontColor": "#7F7F7F",
                        //"xAxisName": "",
                        "yAxisName": "风速 (RPM)",
                        //"yAxisMinValue":"<?php //echo $tmark['min']['value'] - 3?>", 
                        //"yAxisMaxValue":"40<?php //echo $tmark['max']['value'] + 15?>",
                    },                            
                    "data": <?php echo json_encode($ourdoor_data_array['w']) ?>
                }
            });
            outdoor_w_chart.render();
        });
    </script>
  </body>
</html>