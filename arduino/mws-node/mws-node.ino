#include <OneWire.h>
#include <dht.h>
#include <SPI.h>
#include "RF24.h"
#include "printf.h"
#include <EEPROM.h>
#include <MsTimer2.h>

#define ARDUINO_PRO_MINI
//#define ARDUINO_UNO

#ifdef  ARDUINO_PRO_MINI
/* pinouts for pro-mini module */
#define RF24_CE_PIN   9
#define RF24_CS_PIN   10
#define DS18B20_PIN   A0
#define DHT11_PIN     A1
/* pinout for bare atmega328p(uno) */
#elif defined ARDUINO_UNO
#define RF24_CE_PIN   9
#define RF24_CS_PIN   10
#define DHT11_PIN     A1
#define DS18B20_PIN   A2
#define RAINY_PW_PIN  2
#define RAINY_PIN     A0
#define EXT_INT_PIN   3
#endif



#define LOWER_CASE(c) \
  ((c >= 'A' && c <= 'Z') ? c + 0x20 : c)
  
#define IS_HEX_CODE(input) \
  (((input >= '0' && input <= '9') || (input >= 'a' && input <= 'f')) ? true : false)

#define SERROR        0xffff

typedef struct {
  byte rf24_ch;
  uint64_t rf24_addr;
  byte checksum;
} mws_node_id_t;

typedef struct {
  uint16_t humidity;
  uint16_t temperature;
  uint16_t windspeed;
  uint16_t raincode;
} mws_radio_packet_t;

#define MWS_NODE_ID_ADDR   0
#define MWS_SP_ADDR        (MWS_NODE_ID_ADDR + sizeof(mws_node_id_t))

dht DHT;
RF24 radio(RF24_CE_PIN, RF24_CS_PIN);
OneWire  ds(DS18B20_PIN);
mws_radio_packet_t g_rf_pkg = {0, 0, 0, 0};
mws_node_id_t self;
uint16_t windspeed_cache = 0;
uint32_t sampling_period_sec = 0;

void read_system_configuration(byte *rf_ch, uint64_t *rf_addr);
void write_sampling_period(uint32_t period_sec);
void print_device_address(uint64_t ll);
void load_system_configuration(void);
void Ext_Interrupt_Handler(void);

uint16_t ds18b20_read(void);
byte node_checksum(mws_node_id_t *in);
int str2bytes(char *str, byte *buf, int len);
uint64_t bytes2ll(byte *buf, int len);
uint32_t read_sampling_period(void);


void setup() {
  Serial.begin(115200);
  printf_begin();
  
  load_system_configuration();
  
  radio.begin();
  radio.setChannel(self.rf24_ch);
  radio.setAutoAck(1);
  radio.setRetries(15, 15);
  radio.setPALevel(RF24_PA_MAX);
  radio.setCRCLength(RF24_CRC_8);
  radio.setDataRate(RF24_250KBPS);
  radio.enableDynamicPayloads();

  radio.openWritingPipe((uint64_t)0xE7E7E7E7E7LL);
  radio.openReadingPipe(1, self.rf24_addr);

  radio.printDetails();
  radio.startListening();

#ifdef RAINY_PIN
  pinMode(RAINY_PIN, INPUT);
  pinMode(RAINY_PW_PIN, OUTPUT);
  digitalWrite(RAINY_PW_PIN, HIGH);
#endif

#ifdef EXT_INT_PIN
  pinMode(EXT_INT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(EXT_INT_PIN), Ext_Interrupt_Handler, FALLING);
#endif
}

void loop() {
  static unsigned long prev_tick_mark = 0;
  static unsigned long prev_ds18b20_tick = 0;
  bool read_sensor = false;

  listen_radio_packet();

  // millis() will overfloat for 50 days
  if ( prev_tick_mark > millis() ) {
    prev_tick_mark = millis();
  }
  
  uint32_t delta = millis() - prev_tick_mark;
  if ( (prev_tick_mark == 0) || (delta > (sampling_period_sec * 1000)) ) {
    //printf("delta=%d, sp=", delta);
    //Serial.println(sampling_period_sec);
    prev_tick_mark = millis();
    read_sensor = true;
  }

  if (read_sensor) {
    read_sensor = false;    
    // read humidity
    g_rf_pkg.humidity = humidity();
    
#ifdef RAINY_PIN
    g_rf_pkg.raincode = raincode();
#endif

#ifdef EXT_INT_PIN
    g_rf_pkg.windspeed = windspeed();
#endif
    
    if (temperature_start_convert()) {
      g_rf_pkg.temperature = SERROR;
      prev_ds18b20_tick = 0;
    } else {
      prev_ds18b20_tick = millis();
    }
    /*int ret;
    int retry = 5;
    do {
      ret = DHT.read11(DHT11_PIN);
    } while ( (ret != DHTLIB_OK) && (--retry) );
    if (retry == 0) {
      Serial.println("humidity read error");
      g_rf_pkg.humidity = 0xffff;
      //dht11_raw_data = 0xff;
    } else {
      Serial.print("humidity: ");
      Serial.print((float)DHT.humidity, 0);
      Serial.println(" %");
      //dht11_raw_data = DHT.humidity;
      g_rf_pkg.humidity = DHT.humidity;
    }

    // read temperature
    if (ds.reset()) {
      ds.write(0xcc, 1);
      ds.write(0x44, 1);

      prev_ds18b20_tick = millis();
    } else {
      // ds_raw_data = 0xffff;
      g_rf_pkg.temperature = 0xffff;
      prev_ds18b20_tick = 0;
      Serial.println("connect to ds18b20 failed"); 
    }

    // disable the interrupt to read wind-speed
    detachInterrupt(digitalPinToInterrupt(EXT_INT_PIN));
    g_rf_pkg.windspeed = windspeed_cache;
    windspeed_cache = 0;
    attachInterrupt(digitalPinToInterrupt(EXT_INT_PIN), Ext_Interrupt_Handler, FALLING);
    
    Serial.print(F("wind speed: "));
    Serial.print(g_rf_pkg.windspeed);
    Serial.println(F(" rounds"));

    // read the rain-code
    uint32_t avg = 0;
    for (int i = 0; i < 8; i ++) {
      avg += analogRead(RAINY_PIN);
    }
    // power off the rainy sensor
    // digitalWrite(RAINY_PW_PIN, HIGH);
    
    g_rf_pkg.raincode = avg / 8;
    Serial.print(F("rain code: "));
    Serial.print(g_rf_pkg.raincode);
    Serial.println(F("")); */
  }

  if ( (prev_ds18b20_tick) && ((millis() - prev_ds18b20_tick) > 1000) ) {
    /*
    g_rf_pkg.temperature = ds18b20_read();
    if (g_rf_pkg.temperature != 0xffff) {
      Serial.print("temperature: ");
      Serial.print((float)g_rf_pkg.temperature / 16.0, 1);
      Serial.println(" celsius");
    }*/
    g_rf_pkg.temperature = temperature();
    prev_ds18b20_tick = 0;
  }
}

uint16_t humidity(void) {
  int ret;
  int retry = 5;
  
  do {
    ret = DHT.read11(DHT11_PIN);
  } while ( (ret != DHTLIB_OK) && (-- retry) );
  
  if (retry == 0) {
    Serial.println("humidity read error");
    //g_rf_pkg.humidity = 0xffff;
    return SERROR;
  } else {
    //g_rf_pkg.humidity = DHT.humidity;
    Serial.print("humidity: ");
    Serial.print((float)DHT.humidity, 0);
    Serial.println(" %");
    return (uint16_t)DHT.humidity;
  }
}

uint16_t temperature_start_convert(void) {
  if (ds.reset()) {
    ds.write(0xcc, 1);
    ds.write(0x44, 1);
    return 0;
  } else {
    // g_rf_pkg.temperature = 0xffff;
    Serial.println("connect to ds18b20 failed"); 
    return SERROR;
  }
}

uint16_t temperature(void) {
  uint16_t temp = ds18b20_read();
  //g_rf_pkg.temperature = ds18b20_read();
  if (temp != 0xffff) {
    Serial.print("temperature: ");
    Serial.print((float)temp / 16.0, 1);
    Serial.println(" celsius");
    return temp;
  } else {
    return SERROR;
  }
}

#ifdef RAINY_PIN
uint16_t raincode(void) {
  // read the rain-code
  uint32_t avg = 0;
  uint16_t buf;

  digitalWrite(RAINY_PW_PIN, LOW);
  for (int i = 0; i < 8; i ++) {
    avg += analogRead(RAINY_PIN);
  }
  // power off the rainy sensor
  digitalWrite(RAINY_PW_PIN, HIGH);
  
  // g_rf_pkg.raincode = avg >> 3;
  buf = avg >> 3;
  Serial.print(F("rain code: "));
  Serial.print(buf);
  Serial.println(F(""));
  return buf;
}
#endif

#ifdef EXT_INT_PIN
uint16_t windspeed(void) {
  uint16_t buf;
  
  // disable the interrupt to read wind-speed
  detachInterrupt(digitalPinToInterrupt(EXT_INT_PIN));
  //g_rf_pkg.windspeed = windspeed_cache;
  buf = windspeed_cache;
  windspeed_cache = 0;
  attachInterrupt(digitalPinToInterrupt(EXT_INT_PIN), Ext_Interrupt_Handler, FALLING);
  
  Serial.print(F("wind speed: "));
  Serial.print(buf);
  Serial.println(F(" rounds"));
  return buf;
}
#endif

void listen_radio_packet(void) {
  byte rxbuf[32];
  byte txbuf[32] = "";
  int txlen = 0;
  
  if( radio.available() ) {
    int len = radio.getDynamicPayloadSize();
    radio.read(rxbuf, len);
    rxbuf[len] = '\0';
    printf("radio packet: %s\r\n", rxbuf);
   
    radio.stopListening();

    if (strcmp(rxbuf, "hi") == 0) {
      txlen = sizeof(mws_radio_packet_t);
      memcpy(txbuf, &g_rf_pkg, sizeof(mws_radio_packet_t));      
      radio.write(txbuf, txlen);   
      printf("write %d bytes\r\n", txlen);
      
    } else if (rxbuf[0] == 's' && rxbuf[1] == 'p') {
      uint32_t value;
      memcpy(&value, &rxbuf[2], sizeof(uint32_t));
      if (value != sampling_period_sec) {
        sampling_period_sec = value;
        write_sampling_period(value);
        printf("reset samlping period to %d seconds\r\n", value);
      } else {
        printf("samlping period stays %d seconds\r\n", sampling_period_sec);
      }
    }
    radio.startListening(); 
  }  
}

void load_system_configuration(void) {
  EEPROM.get(MWS_NODE_ID_ADDR, self);
  do {    
    if (self.checksum != node_checksum(&self)) {
      //Serial.println(F("No device configruration is found."));
      read_system_configuration(&self.rf24_ch, &self.rf24_addr);
      printf("RF channel set to %d, address set to ", self.rf24_ch);
      print_device_address(self.rf24_addr);
      printf("?(y/n)");
      while (1) {
        if (Serial.available()) {
          char c = Serial.read();
          if (c == 'y') {
            self.checksum = node_checksum(&self);
            EEPROM.put(MWS_NODE_ID_ADDR, self);
            break;
          } else if (c == 'n') {
            break;
          }
        }
      }    
    } else {
      printf("Device RF channel: %d\r\n", self.rf24_ch);
      printf("Device RF Address: ");
      print_device_address(self.rf24_addr);
      Serial.println("");
  
      Serial.print("Input anything to reset ... ");
      int timeout = 15;
      do {
        if (Serial.available()) {
          Serial.flush();
          self.checksum = 0;
          break;
        } else {
          delay(100);
          -- timeout; 
        }
      } while (timeout);
      if (timeout == 0) {
        Serial.println("timeout");
      }
    }
  } while (self.checksum != node_checksum(&self));

  sampling_period_sec = read_sampling_period();
  
  if (sampling_period_sec > (24 * 3600)) {
    Serial.println(F("error: sampling period more than 24 hours, reset to 10 minites"));
    sampling_period_sec = 10 * 60;
    write_sampling_period(sampling_period_sec);
  } else {
    printf("Sampling period is %d seconds\r\n", sampling_period_sec);
  }
}

uint32_t read_sampling_period(void) {
  uint32_t buf;
  
  EEPROM.get(MWS_SP_ADDR, buf);
  return buf;
}

void write_sampling_period(uint32_t period_sec) {
  EEPROM.put(MWS_SP_ADDR, period_sec);
}

void read_system_configuration(byte *rf_ch, uint64_t *rf_addr) {
  int ch;
  char str[16];
  char llbuf[5];
  
  //Serial.println(F("No device configruration is found."));
  //Serial.setTimeout(-1);
  do {
    Serial.flush();
    Serial.println(F("Input the RF channel(1-127): "));
    while (!Serial.available());
    delay(100);
    ch = Serial.parseInt();
    if (ch > 127 || ch < 0) {
      Serial.println(F("error: RF channel must between 1 to 127."));
    }
  } while (ch > 127 || ch <= 0);
  //printf("RF channel set to %d\r\n", ch);
  *rf_ch = ch;
 
  while (1) {
    Serial.println(F("Input the RF address(40bit): "));
    for (int i = 0; i < 10;) {
      if (Serial.available()) {
        str[i] = Serial.read();
        if (str[i] != '\r' && str[i] != '\n' && str[i] != ' ')
          i ++;
      }
    }
    if (Serial.available()) {
      Serial.flush();
    }
    str[10] = '\0';
    //printf("what you input is: '%s'\r\n", str);
    if (str2bytes(str, llbuf, 5) < 0) {
      Serial.println(F("error: wrong RF address(40bit)"));
    } else {
      //self.rf24_addr = bytes2ll(llbuf, 5);
      *rf_addr = bytes2ll(llbuf, 5);
      break;
    }
  }
}

int str2bytes(char *str, byte *buf, int len) {
  uint8_t cache;
  
  for (int i = 0; i < (len * 2); i += 2) {
    cache = 0;
    for(int j = i; j < (i + 2); j ++) {
      cache <<= 4;
      str[j] = LOWER_CASE(str[j]);
      //printf("str[%d]=0x%x,cache=", j, str[j]);
      if (str[j] >= '0' && str[j] <= '9') {
        cache |= (str[j] - '0') & 0xf;
      } else if (str[j] >= 'a' && str[j] <= 'f') {
        cache |= (str[j] - 0x57) & 0xf;
      } else {
        return -1;
      }
      //printf("0x%x\r\n", cache);
    }
    //printf("buf[%d]=0x%x\r\n", i/2, buf[i/2]);
    //*(buf ++) = cache;
    buf[i/2] = cache;
  }
  return 0;
}

void ll2bytes(uint64_t ll, byte buf[8]) {
  for (int i = 0; i < 8; i ++) {
    buf[i] = ll & 0xff;
    ll >>= 8;
  }
}

uint64_t bytes2ll(byte *buf, int len) {
  uint64_t ll = 0;
  for (int i = 0; i < len; i ++) {
    ll <<= 8;
    ll |= buf[i];
  }
  return ll;
}

byte node_checksum(mws_node_id_t *in) {
  byte buf[8];
  byte sum = in->rf24_ch;
  
  ll2bytes(in->rf24_addr, buf);
  for(int i = 0; i < 8; i ++) {
    sum += buf[i];  
  }
  return sum;
} 

void print_device_address(uint64_t ll) {
  byte buf[8];

  ll2bytes(ll, buf);
  for (int i = 4; i >= 0; i --) {
    printf("%02x", buf[i]);
  }
}

uint16_t ds18b20_read(void) {
  byte data[12];
  
  ds.reset();
  ds.write(0xcc, 1);  
  ds.write(0xBE);
  
  for (int i = 0; i < 9; i++) {           
    // we need 9 bytes
    data[i] = ds.read();
  }

  if (OneWire::crc8(data, 8) != data[8]) {
    Serial.println(F("Read DS18B20 CRC error"));
    return 0xffff;
  }

  return ((data[1] << 8) | data[0]);
}

#ifdef EXT_INT_PIN
void Ext_Interrupt_Handler(void)  {
  windspeed_cache ++;
}
#endif

