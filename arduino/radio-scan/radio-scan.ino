#include <SPI.h>

#include "RF24.h"
#include "printf.h"
#include "nRF24L01.h" 

#define RF24_CE_PIN   9
#define RF24_CS_PIN   10

RF24 g_radio(RF24_CE_PIN, RF24_CS_PIN);

void setup() {
  Serial.begin(115200);
  printf_begin();
  Serial.println(F("Input 'T' to start the scan"));
}

void loop() {
  unsigned int buf[126];
  
  if (Serial.available()) {
    char c = toupper(Serial.read());
    if (c == 'T') {
      read_cd_values(buf, 500);
    } else if (c == 'B') {
      freq_blockage(70); 
    } else {
      Serial.println(F("Input 'T' to start the scan"));
    }
  }

}

void radio_start(int ch) {
  g_radio.begin();
  g_radio.setChannel(ch);
  g_radio.setPALevel(RF24_PA_MAX);
  g_radio.setAutoAck(1);                    
  g_radio.enableAckPayload();               
  g_radio.setRetries(15, 15);                
  g_radio.setCRCLength(RF24_CRC_8);
  g_radio.setDataRate(RF24_250KBPS);
  g_radio.enableDynamicPayloads();
  //g_radio.setPayloadSize(4);                
  //g_radio.openWritingPipe(pipes[1]); 
  g_radio.openWritingPipe(0x544d52687CLL);       
  //g_radio.openReadingPipe(1,pipes[0]);
  g_radio.openReadingPipe(1, 0xABCDABCD71LL);
  g_radio.startListening();
}

void radio_stop(void) {
  g_radio.stopListening();
}

void read_cd_values(unsigned int *buf, int period_ms) {
  unsigned long tick;
  
  for (int ch = 0; ch < 126; ch ++) {
    radio_start(ch);
    buf[ch] = 0;
    tick = millis();
    do {
      if (g_radio.testRPD()) {
        buf[ch] ++;
      }
    } while ( (millis() - tick) < period_ms);
    
    //printf("%d: %d\r\n", ch, buf[ch]);
    if (ch > 0 && (ch % 16) == 0) {
      printf("\r\n");
    }
    printf("%3d ", buf[ch]);    
    radio_stop();
    delay(10);
  }
  printf("\r\n");
}

void freq_blockage(int ch) {
  byte txbuf[32];

  memset(txbuf, 0xAA, 32);
  
  g_radio.begin();
  g_radio.setChannel(ch);
  g_radio.setPALevel(RF24_PA_MAX);
  g_radio.setAutoAck(0);                    
  //g_radio.enableAckPayload();   
  g_radio.setPayloadSize(32);            
  g_radio.setRetries(0, 0);                
  g_radio.setCRCLength(RF24_CRC_8);
  g_radio.setDataRate(RF24_2MBPS);
  g_radio.openWritingPipe(0xaaaaaaaaaaLL);       
  g_radio.openReadingPipe(1, 0x5555555555LL);
  //g_radio.startListening();
  g_radio.stopListening();
  printf("blocking frequency channel %d, will never return\r\n", ch);
  while (1) {  
    g_radio.write(txbuf, 32);
  }
}

